![](https://images.microbadger.com/badges/version/jarylc/duoauthproxy.svg) ![](https://images.microbadger.com/badges/image/jarylc/duoauthproxy.svg) ![](https://img.shields.io/docker/stars/jarylc/duoauthproxy.svg) ![](https://img.shields.io/docker/pulls/jarylc/duoauthproxy.svg)

# Volumes
- /app/conf/authproxy.cfg - configuration file (https://duo.com/docs/authproxy_reference)

Note: It is recommended to have
```toml
[main]
log_stdout=true
```
if you would like Docker to capture logs.

# Deploying
## Terminal
```bash
docker run -d \
    --name duoauthproxy \
    -p 1812:1812 \
    -v /path/to/authproxy.cfg:/app/conf/authproxy.cfg \
    --restart unless-stopped \
    minimages/duoauthproxy
```
## Docker-compose
```yml
duoauthproxy:
    image: minimages/duoauthproxy
    ports:
        - "1812:1812"
    volumes:
        - /path/to/authproxy.cfg:/app/conf/authproxy.cfg
    restart: unless-stopped
```

## Examples
You can visit the [examples folder in this repository](examples) for example deployments.
