ARG PYTHON_VERSION
ARG ALPINE_VERSION

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION} AS builder

ARG VERSION
ARG CHECKSUM
ARG CRYPTOGRAPHY_OPENSSL_NO_LEGACY=1

RUN apk add --no-cache build-base libffi-dev perl zlib-dev diffutils libstdc++ gcompat \
                       bash wget py3-cffi py3-zope-interface py3-cryptography
RUN wget -O duoauthproxy.tgz https://dl.duosecurity.com/duoauthproxy-${VERSION}-src.tgz
RUN echo "${CHECKSUM} duoauthproxy.tgz" | sha256sum -c
RUN tar xzf duoauthproxy.tgz
WORKDIR duoauthproxy-${VERSION}-src
RUN mkdir -p duoauthproxy-build/usr/local/lib/python${PYTHON_VERSION%.*}/
RUN cp -R /usr/lib/python${PYTHON_VERSION%.*}/site-packages duoauthproxy-build/usr/local/lib/python${PYTHON_VERSION%.*}/
RUN sed -i '/$(CFFI) \\/d' Makefile
RUN sed -i '/$(ZOPE_INTERFACE) \\/d' Makefile
RUN sed -i '/$(CRYPTOGRAPHY) \\/d' Makefile
RUN make
RUN LD_PRELOAD=libgcompat.so.0 duoauthproxy-build/install --install-dir /app --service-user nobody --log-group nobody --create-init-script no --enable-selinux=no
RUN rm -rf /app/usr/local/lib/python${PYTHON_VERSION%.*}/test
RUN chown -R nobody:nobody /app/conf

FROM alpine:edge
WORKDIR /app
RUN apk add --no-cache libgcc libffi-dev gcompat su-exec
USER 0:0
COPY --from=builder /app /app
COPY entrypoint.sh /app/entrypoint.sh
ENTRYPOINT ["/app/entrypoint.sh"]
