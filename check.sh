#!/bin/bash

apk add curl jq htmlq

[[ ! -f EXISTING ]] || touch EXISTING
EXISTING=$(cat EXISTING)
echo "Existing: ${EXISTING}"

INFO=$(curl -s https://duo.com/docs/checksums | grep 'https://dl.duosecurity.com/duoauthproxy' | grep 'src.tgz' | rev | cut -d'>' -f1 | rev)
CHECKSUM=$(echo "$INFO" | cut -d' ' -f1)
LATEST=$(echo "$INFO" | cut -d'-' -f2)
PYTHON_VERSION=$(curl -s https://duo.com/docs/authproxy-notes | grep -Eo 'Python to \d+\.\d+\.\d+' | head -n1 | rev | cut -d' ' -f1 | rev)
ALPINE_VERSION='edge'
for ALPINE_VERSION in $(curl -s "https://pkgs.alpinelinux.org/packages?name=python${PYTHON_VERSION%.*.*}" | htmlq -w --text 'select#branch > option' | grep -v 'Branch' | grep -v 'edge' | tac | xargs); do
    if curl -s "https://pkgs.alpinelinux.org/packages?name=python${PYTHON_VERSION%.*.*}&branch=${ALPINE_VERSION}" | grep -q ${PYTHON_VERSION%.*}; then
        ALPINE_VERSION="${ALPINE_VERSION/v/}"
        break
    fi
done

echo "Latest: ${LATEST}"
echo "Checksum: ${CHECKSUM}"
echo "Python Version: ${PYTHON_VERSION}"
echo "Alpine Version: ${ALPINE_VERSION/v/}"

if [[ (-n "${LATEST}" && "${LATEST}" != "${EXISTING}") ]]; then
  mv build.template.yml build.yml
  sed -i "s \$CHECKSUM ${CHECKSUM} g" 'build.yml'
  sed -i "s \$LATEST ${LATEST} g" 'build.yml'
  sed -i "s \$PYTHON_VERSION ${PYTHON_VERSION} g" 'build.yml'
  sed -i "s \$ALPINE_VERSION ${ALPINE_VERSION} g" 'build.yml'
  echo "Building..."
fi
