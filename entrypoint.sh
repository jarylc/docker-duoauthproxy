#!/bin/ash
set -e
export LD_PRELOAD=libgcompat.so.0
export CRYPTOGRAPHY_OPENSSL_NO_LEGACY=1
su-exec 0:0 /app/bin/authproxy_connectivity_tool
su-exec 0:0 /app/bin/authproxy
